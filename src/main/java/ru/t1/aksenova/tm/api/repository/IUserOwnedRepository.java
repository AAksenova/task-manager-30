package ru.t1.aksenova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeOne(@Nullable String userId, @Nullable M model);

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

}
