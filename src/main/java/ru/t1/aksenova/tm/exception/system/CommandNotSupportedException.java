package ru.t1.aksenova.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! This \"" + command + "\" is not supported...");
    }

}
