package ru.t1.aksenova.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! This \"" + argument + "\" is not supported...");
    }

}
