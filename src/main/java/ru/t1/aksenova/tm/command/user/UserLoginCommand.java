package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.service.IAuthService;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "login";

    @NotNull
    public static final String DESCRIPTION = "User login.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        authService.login(login, password);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
