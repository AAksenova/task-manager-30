package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;

        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
