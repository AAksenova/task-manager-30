package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();

        System.out.println("ENTER TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();

        getTaskService().updateByIndex(userId, index, name, description);
    }

}
